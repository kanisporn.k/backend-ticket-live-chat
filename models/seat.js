const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Seat = new Schema({  
  seat: String,
  status: String,
  price: String
})
module.exports = mongoose.model('Seat', Seat)
