const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Movie = new Schema({  
  movie_name: String,
  genra: String,
  rate: String,
  time: String,
  release_date: String,
  datail: String,
  image: String
})

module.exports = mongoose.model('Movie', Movie)
