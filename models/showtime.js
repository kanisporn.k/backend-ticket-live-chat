const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Showtime = new Schema({
  date: String, showtime1:{time: String, seat1: {name_seat: String, status: Boolean, price: String}, seat2: {name_seat: String, status: Boolean, price: String}},
  showtime2:{time: String, seat1: {name_seat: String, status: Boolean, price: String}, seat2: {name_seat: String, status: Boolean, price: String}}

})
module.exports = mongoose.model('showtime', Showtime)
