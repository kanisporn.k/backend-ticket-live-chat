const mongoose = require('mongoose')
const Schema = mongoose.Schema
const User = new Schema({
  firstname: String,
  lastname: String,
  username: String,
  password: String,
  token: String
})
module.exports = mongoose.model('Users', User)
