var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
const mongoose = require('mongoose')
require('dotenv').config()
var cors = require('cors')
var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var movieRouter = require('./routes/movie')
var seatRouter = require('./routes/seat')
var showtimeRouter = require('./routes/showtime')
var app = express()
let http = require('http').Server(app);
let io = require('socket.io')(http);

app.get('/chat', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})


http.listen(3005, () => {
    console.log('Chat_modules Listening on port *: 3005');
})

io.on('connection', (socket) => {

    socket.emit('connections', Object.keys(io.sockets.connected).length);

    socket.on('disconnect', () => {
        console.log("A user disconnected");
    })

    socket.on('chat-message', (data) => {
        socket.broadcast.emit('chat-message', (data));
    })

    socket.on('typing', (data) => {
        socket.broadcast.emit('typing', (data));
    })

    socket.on('stopTyping', () => {
        socket.broadcast.emit('stopTyping');
    })

    socket.on('joined', (data) => {
        socket.broadcast.emit('joined', (data));
    })

    socket.on('leave', (data) => {
        socket.broadcast.emit('leave', (data));
    })

})


var { DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS } = process.env
mongoose.connect(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`, {
  user: `${DB_USER}`,
  pass: `${DB_PASS}`,
  useNewUrlParser: true,
  useCreateIndex: true,
  authMechanism: 'SCRAM-SHA-1'
})

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/', usersRouter)
app.use('/', movieRouter)
app.use('/', seatRouter)
app.use('/', showtimeRouter)
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})


module.exports = app
