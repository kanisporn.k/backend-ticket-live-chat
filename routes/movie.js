var express = require('express')
var router = express.Router()
const Movie = require('../models/movie')

router.post('/movie', function (req, res, next) {
  let newmovie = new Movie({    
    movie_name: req.body.movie_name,
    genra: req.body.genra,
    rate: req.body.rate,
    time: req.body.time,
    release_date: req.body.release_date,
    datail: req.body.datail,
    image: req.body.image
  })
  newmovie.save()
  res.json({ Message: 'Create Succeed' })
})

router.get('/movie', async (req, res, next) => {
  try {
    res.send(await Movie.find())
  } catch (error) {
    next(error)
  }
})

router.get('/movie/:id', async (req, res, next) => {
  try {
    res.send(await Movie.findOne({ _id: req.params.id }))
  } catch (error) {
    next(error)
  }
})

router.put('/movie/:id', async (req, res, next) => {
  try {
    let newmovie = new Movie({    
      movie_name: req.body.movie_name,
      genra: req.body.genra,
      rate: req.body.rate,
      time: req.body.time,
      release_date: req.body.release_date,
      datail: req.body.datail,
      image: req.body.image
    })
    newmovie.save()
  } catch (error) {
    next(error)
  }
})

router.delete('/movie/:id', async (req, res, next) => {
  try {
    let movie = await User.findOne({ _id: req.params.id })
    await user.delete()
    res.send({ message: 'delete succeed' })
  } catch (error) {
    next(error)
  }
})

module.exports = router
