var express = require('express')
var router = express.Router()
const Seat = require('../models/seat')
/* POST users listing. */
router.post('/seat', function (req, res, next) {
  let newseat = new Seat({
  })
  newseat.save()
  res.json({ Message: 'Create Succeed' })
})

router.get('/seat', async (req, res, next) => {
  try {
    res.send(await Seat.find())
  } catch (error) {
    next(error)
  }
})

router.get('/seat/:id', async (req, res, next) => {
  try {
    res.send(await Seat.findOne({ _id: req.params.id }))
  } catch (error) {
    next(error)
  }
})

module.exports = router