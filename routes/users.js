var express = require('express')
var router = express.Router()

var jwt = require('jsonwebtoken')
const session = require('express-session')
const bcrypt = require('bcrypt')
const saltRounds = 10
const myPlaintextPassword = 's0/\/\P4$$w0rD'
const someOtherPlaintextPassword = 'not_bacon'
const User = require('../models/user')
/* POST users listing. */
router.post('/user', async (req, res, next) => {
  console.log(req.body)
  const password = req.body.password
  const passwordHash = await bcrypt.hash(password, 8);

  let newuser = new User({    
    username: req.body.username,
    password: passwordHash,   
  })
  newuser.save()
  res.json({ Message: 'Sign in Succeed' })
})

router.get('/login', async (req, res, next) => {
  try {
    res.json({ Message: 'Login Succeed' })
  } catch (error) {
    next(error)
  }
})

router.get('/user', async (req, res, next) => {
  try {
    res.send(await User.find())
  } catch (error) {
    next(error)
  }
})

router.get('/user', async (req, res, next) => {
  try {
    res.send(await User.find())
  } catch (error) {
    next(error)
  }
})


// ตรวจสอบ user , password ตอน login

router.post('/login', async (req, res, next) => {
  try {
    let user = await User.findOne({username: req.body.username})
    const isValid = await bcrypt.compare(req.body.password, user.password)
    var token = jwt.sign({ _id: user.id, username: user.username }, 'tokenn')
    user.token = token
    user.save()
    const objUser = user.toObject()
    delete objUser.password
    res.send({objUser, isValid})  
  } catch (error) {
    next(error)
  }
})

// GET /logout
router.post('/logout', async (req,res) => {
  try {
    const decode = await jwt.verify(req.headers.token, 'tokenn')
    const user = await User.findById({_id: decode._id})
    user.token = ""
    await user.save()
    res.send(user)
  } catch (error) {
    res.send('error')
  }

  
});

router.get('/user/:id', async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.params.id })
    const isValid = await bcrypt.compare('12323dsffaำพ4', user.password)
    res.send({user, isValid})
  } catch (error) {
    next(error)
  }
})

router.put('/user/:id', async (req, res, next) => {
  try {
    let user = await User.findOne({ _id: req.params.id })
    user.age = req.body.age
    user.firstname = req.body.firstname
    user.lastname = req.body.lastname
    user.save()
    res.send({ Message: 'update succeed' })
  } catch (error) {
    next(error)
  }
})

router.put('/users', async (req, res, next) => {
  try {
    let users = await User.find()
    for (const user of users) {
      if (req.body.age) {
        user.age = req.body.age
        user.firstname = req.body.firstname
        user.lastname = req.body.lastname
      }
      await user.save()
    }
    res.send({ Message: 'update succeed' })
  } catch (error) {
    next(error)
  }
})

router.delete('/user/:id', async (req, res, next) => {
  try {
    let user = await User.findOne({ _id: req.params.id })
    await user.delete()
    res.send({ message: 'delete succeed' })
  } catch (error) {
    next(error)
  }
})

module.exports = router