var express = require('express')
var router = express.Router()
const Showtime = require('../models/showtime')
/* POST users listing. */
router.post('/showtime', function (req, res, next) {
  let newshowtime1 = new Showtime({
    
  date: '15June19', showtime1:{time: '13:00', seat1: {name_seat: 'A1', status: false, price: '150'}, seat2: {name_seat: 'A2', status: false, price: '150'}},
  showtime2:{time: '16:00', seat1: {name_seat: 'A1', status: false, price: '150'}, seat2: {name_seat: 'A2', status: false, price: '150'}}


  })
  newshowtime1.save()

  res.json({ Message: 'Create Succeed' })
})

router.get('/showtime', async (req, res, next) => {
  try {
    res.send(await Showtime.find())
  } catch (error) {
    next(error)
  }
})

router.get('/showtime/:id', async (req, res, next) => {
  try {
    res.send(await Showtime.findOne({ _id: req.params.id }))
  } catch (error) {
    next(error)
  }
})

module.exports = router